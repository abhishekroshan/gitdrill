function lastCar(inventory){
    return inventory[inventory.length-1];  //using the .length funtion to get to the last index of inventory
}

module.exports = lastCar;

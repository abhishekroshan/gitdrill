function carSort(inventory){
    const newInventory = [];      //create new array to store names

    for(let i=0;i<inventory.length;i++){
        newInventory.push(inventory[i].car_model);  //pushing car names to array
    }

    newInventory.sort();  //sorting array

    return newInventory;
}

module.exports = carSort;
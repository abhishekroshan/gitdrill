
function oldCars(allCars){
    
    let count = 0;

    for(let i=0;i<allCars.length;i++){
        if(allCars[i]<2000){
            count++;
        }
    }
    return count;
}

module.exports = oldCars;
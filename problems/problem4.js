function carYear(inventory){
    const years= [];      //create new array to store years

    for(let i=0;i<inventory.length;i++){
        years.push(inventory[i].car_year);  //pushing car years to array
    }

    return years;
}

module.exports = carYear;
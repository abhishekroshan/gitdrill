function findCar(inventory,id){
    for(let i=0;i<inventory.length;i++){
        if(inventory[i].id === id){   //iterating over inventory to 
            return inventory[i];      //find the required id
        }
    }
}

module.exports = findCar;
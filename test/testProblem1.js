const findCar = require("../problems/problem1");  
const inventory = require('../inventory');       //importing inventory

const result = findCar(inventory,33);  //storing the result

console.log(`Car 33 is a ${result.car_year} ${result.car_make} ${result.car_model}`);
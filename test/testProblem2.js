const lastCar = require("../problems/problem2");
const inventory = require("../inventory");

const result = lastCar(inventory);

console.log(`Last car is a ${result.car_make} ${result.car_model}`);

